FROM portown/alpine-pandoc

RUN apk add --update mc nodejs bash findutils && rm -f /var/cache/apk/*

RUN npm install -g esdoc \
	esdoc-standard-plugin \
	esdoc-react-plugin \
	esdoc-publish-html-plugin \
	esdoc-undocumented-identifier-plugin \
	esdoc-ecmascript-proposal-plugin \
	esdoc-exclude-source-plugin \
	esdoc-external-nodejs-plugin \
	esdoc-external-webapi-plugin \
	esdoc-flow-type-plugin \
	esdoc-importpath-plugin \
	esdoc-inject-script-plugin \
	esdoc-inject-style-plugin \
	esdoc-integrate-manual-plugin \
	esdoc-integrate-test-plugin \
	esdoc-jsx-plugin \
	esdoc-publish-markdown-plugin \
	esdoc-type-inference-plugin \
	esdoc-typescript-plugin \
	esdoc-unexported-identifier-plugin \
	esdoc-brand-plugin \
	esdoc-external-ecmascript-plugin \
	esdoc-type-inference-plugin \
	esdoc-lint-plugin \
	esdoc-accessor-plugin \
	esdoc-coverage-plugin \
	pandoc-mermaid-filter

COPY docs-to-html.sh /usr/local/bin/docs-to-html.sh
RUN chmod 777 /usr/local/bin/docs-to-html.sh

COPY templates/ /data/templates/

WORKDIR /data

ENTRYPOINT docs-to-html.sh