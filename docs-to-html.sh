#!/bin/bash

mkdir -p ./dist/data
cp -r ./src/data/* ./dist/data
cp ./templates/pandoc.css ./dist/pandoc.css
pages=()

for d in `find ./src/ -mindepth 1 -name '*.md' -printf '%P\n'`; do
	mkdir -p ./dist/$(dirname "${d}")
	pandoc ./src/$d -t html -F pandoc-mermaid --template=./templates/template.html --css ../pandoc.css -o ./dist/${d%%.*}.html
	pages+=("<li><a href='./${d%%.*}.html' target='content'>${d%%.*}</a></li>")
	echo "${d%%.*}"
done

links=$(printf "\n%s" "${pages[@]}")

cat ./templates/index.html | awk -v "LINKS=$links" '{gsub(/NAVIGATION/,LINKS)}1' > ./dist/index.html